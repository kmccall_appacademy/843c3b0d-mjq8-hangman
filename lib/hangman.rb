class Hangman
  attr_reader :guesser, :referee, :board, :dictionary, :wrong_letters, :wrong_guesses

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @wrong_letters = []
    @wrong_guesses = 0
  end
  def play
    setup
    display
    until won?
      take_turn
      display
      if @wrong_guesses > 9
        puts "10 wrong guesses.  #{@guesser} loses. boohoo"
        exit
      end
    end

    if won?
      puts "woohoo #{@guesser} wins with #{@wrong_guesses} wrong guesses!!"
    end
  end

  def display
    @board.each do |el|
      print "_" if el == nil
      print el if el
    end
    puts ""
  end

  def won?
    @board.all?
  end

  def guesser
    @guesser
  end

  def referee
    @referee
  end

  def setup
    secret_word_length = @referee.pick_secret_word
    @board = Array.new(secret_word_length)
    @guesser.register_secret_length(secret_word_length)
  end

  def update_board(letter, indices)
    indices.each {|idx| @board[idx] = letter}
  end

  def take_turn
    letter = @guesser.guess(@board)
    indices = @referee.check_guess(letter)

      if indices == []
        @wrong_letters << letter
        @wrong_guesses += 1
      else
        update_board(letter, indices)
        #puts @board
      end
      @guesser.handle_response(letter, indices)
  end

end

class HumanPlayer
  def initialize(dictionary)
    @dictionary = dictionary
    @secret_word = ""
  end
  def pick_secret_word
    puts "Think of a secret word please"
    puts "Type how many letters long it is:"
    gets.chomp.to_i
  end
  def guess(board)
    puts "Please guess a letter:"
    gets.chomp
  end
  def register_secret_length(length)
    @secret_word_length = length
  end

  def check_guess(letter)
    puts "The guess was: #{letter}"
    puts "Where, if anywhere does that occur?"
    puts "(put indices or hit enter if it does not occur)"
    gets.chomp.split(", ").map { |el| Integer(el)}

  end

  def handle_response(letter, indices)
   puts "That letter is not in the secret word!" if indices.empty?
   puts "Nice! '#{letter}' is found the following
    location(s): #{indices}" unless indices.empty?
  end
end

class ComputerPlayer
  attr_reader :candidate_words
  attr_accessor :secret_word

  def initialize(dictionary = "dictionary.txt")
    #if dictionary == "dictionary.txt"
      @dictionary = File.readlines(dictionary).map(&:chomp)
    #else
    #@dictionary = dictionary
    #end
    #@secret_word = ""
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def candidate_words
    @candidate_words
  end

  def register_secret_length(length)
    @secret_word_length = length
    @candidate_words = @dictionary.select {|word| word.length == @secret_word_length}
  end

  def secret_word
    @secret_word
  end

  def guess(board)
    total_letters = Hash.new(0)
    @candidate_words.each do |word|
      word.chars.each.with_index do |ch, idx|
      total_letters[ch] +=1 if board[idx].nil?
      end
    end
    total_letters.sort_by {|key, value| value }[-1][0]
  end

  def handle_response(letter, indices)
    if indices.empty?
      @candidate_words.reject! {|word| word.include?(letter)}
    else
      indices.each do |idx|
        @candidate_words.select! {|word| word[idx] == letter}
      end
        @candidate_words.select! {|word| word.count(letter) == indices.length}
    end
  end

  def check_guess(letter)
    result = []
    @secret_word.each_char.with_index do |ch, i|
      result << i if ch == letter
    end
    result
  end
end

if __FILE__ == $PROGRAM_NAME
  puts "Welcome to Hangman!"
  puts "Do you want to be the guesser or referee?"
  position = gets.chomp
    if position == "referee"
      guesser = ComputerPlayer.new("dictionary.txt")
      referee = HumanPlayer.new("dictionary.txt")
      puts "The Computer has 10 tries to guess your word"
    elsif position =="guesser"
      referee = ComputerPlayer.new("dictionary.txt")
      guesser = HumanPlayer.new("dictionary.txt")
      puts "You have 10 tries to guess the Computers word"
    else
      puts "I didn't get that. Bye"
      exit
    end
  Hangman.new({guesser: guesser, referee: referee}).play
end
